<?php
date_default_timezone_set('Europe/Moscow');
mb_internal_encoding("UTF-8");
require __DIR__ . '/vendor/autoload.php';

$checker = new ClientChecker\CheckList();

//GET REQUEST
$getParams = $_GET;
if (isset($getParams['method'])) {
    $methods = explode(',', $getParams['method']);
    $inn = (isset($getParams['inn'])) ? explode(',', $getParams['inn']) : [];
    $passportSerial = (isset($getParams['passport_serial'])) ? explode(',', $getParams['passport_serial']) : [];
    $passportNumber = (isset($getParams['passport_number'])) ? explode(',', $getParams['passport_number']) : [];
    $bik = (isset($getParams['bik'])) ? explode(',', $getParams['bik']) : [];

    foreach ($methods as $method) {
        switch ($method) {
            case ClientChecker\CheckInstance\Bankrupt::GetMethodName():
                if (!empty($inn)) {
                    foreach ($inn as $innItem) {
                        $checker->AddCheckerInstance(ClientChecker\CheckInstance\Bankrupt::Init(['inn' => $innItem]));
                    }
                }
                break;
            case ClientChecker\CheckInstance\Rnp::GetMethodName():
                if (!empty($inn)) {
                    foreach ($inn as $innItem) {
                        $checker->AddCheckerInstance(ClientChecker\CheckInstance\Rnp::Init(['inn' => $innItem]));
                    }
                }
                break;
            case ClientChecker\CheckInstance\Msp::GetMethodName():
                if (!empty($inn)) {
                    foreach ($inn as $innItem) {
                        $checker->AddCheckerInstance(ClientChecker\CheckInstance\Msp::Init(['inn' => $innItem]));
                    }
                }
                break;
            case ClientChecker\CheckInstance\Passport::GetMethodName():
                if (!empty($passportSerial) && !empty($passportNumber) && count($passportSerial) == count($passportNumber)) {
                    for ($i = 0, $count = count($passportNumber); $i < $count; $i++) {
                        $checker->AddCheckerInstance(ClientChecker\CheckInstance\Passport::Init([
                            'series' => $passportSerial[$i], 'number' => $passportNumber[$i]]));
                    }
                }
                break;
            case ClientChecker\CheckInstance\Debt::GetMethodName():
                if (!empty($inn)) {
                    foreach ($inn as $innItem) {
                        $checker->AddCheckerInstance(ClientChecker\CheckInstance\Debt::Init(['inn' => $innItem]));
                    }
                }
                break;
            case ClientChecker\CheckInstance\AccBlock::GetMethodName():
                if (!empty($inn) && !empty($bik) && count($inn) == count($bik)) {
                    for ($i = 0, $count = count($inn); $i < $count; $i++) {
                        $checker->AddCheckerInstance(ClientChecker\CheckInstance\AccBlock::Init([
                            'inn' => $inn[$i], 'bik' => $bik[$i]]));
                    }
                }
                break;
        }
    }
} else {
    //POST REQUEST
    $postParams = $_POST;
}

$checkResult = $checker->RunCheck();
echo json_encode($checkResult);
