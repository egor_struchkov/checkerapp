<?php

namespace ClientChecker;

use ClientChecker\CheckInstance\ICheckInstance;

class CheckList
{
    private $instances = [];
    private $checkResult = [];

    public function AddCheckerInstance(ICheckInstance $instance)
    {
        $this->instances[] = $instance;
    }

    public function RunCheck()
    {
        foreach ($this->instances as $instance) {
            try {
                $this->checkResult[] = [
                    'method' => $instance::GetMethodName(),
                    'result' => $instance->Check(),
                    'status' => true
                ];
            } catch (\Exception $e) {
                $this->checkResult[] = [
                    'method' => $instance::GetMethodName(),
                    'result' => false,
                    'status' => false
                ];
            }

        }

        return $this->checkResult;
    }
}