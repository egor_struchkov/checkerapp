<?php

namespace ClientChecker;


class CaptchaResolve
{
    const MAX_REQUEST_COUNT = 5;    //максимальное число запросов для получения результата от api rucaptcha
    const REQUEST_DELAY = 5;        //интервал между попытками в секундах
    const API_JOB_URL = 'http://rucaptcha.com/in.php';
    const API_RES_URL = 'http://rucaptcha.com/res.php';
    const API_KEY = '20b9d884c1479b6ab31624c8156681fe';

    public static function Resolve($dataImg)
    {
        $ch = curl_init(self::API_JOB_URL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'method' => 'base64',
            'key' => self::API_KEY,
            'body' => base64_encode($dataImg),
            'json' => 1,
        ]));
        $output = curl_exec($ch);
        curl_close($ch);
        $captchaJobAnswer = json_decode($output, true);
        $jobStatus = $captchaJobAnswer['status'];
        if ($jobStatus != 1)
            throw new \Exception('Captcha api error, job status fail');
        $jobId = $captchaJobAnswer['request'];
        $jobResult = self::GetJobResult($jobId);

        return $jobResult['request'];
    }

    protected static function GetJobResult($jobId, &$requestCount = 0)
    {
        //get captcha solver
        $requestCount++;
        sleep(self::REQUEST_DELAY);
        $ch = curl_init(self::API_RES_URL . "?key=" . self::API_KEY . "&action=get&id={$jobId}&json=1");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        $captchaAnswer = json_decode($output, true);
        if ($captchaAnswer['status'] != 1) {
            if ($requestCount > self::MAX_REQUEST_COUNT)
                throw new \Exception('Resolve captcha error. Maximum number of requests exceeded.');
            return self::GetJobResult($jobId, $requestCount);
        }

        return $captchaAnswer;
    }
}