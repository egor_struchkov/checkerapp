<?php
namespace ClientChecker\CheckInstance;

use PhpQuery\PhpQuery as phpQuery;

class Rnp implements ICheckInstance
{
    private $data = [
        'inn' => ''
    ];

    public static function Init(array $dataInput)
    {
        $inst = new self();
        $inst->SetDataInput($dataInput);
        return $inst;
    }

    public static function GetMethodName()
    {
        return 'rnp';
    }

    public function GetDataUrl()
    {
        return "http://zakupki.gov.ru/epz/dishonestsupplier/quicksearch/search.html?searchString={$this->data['inn']}&morphology=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&fz94=on&fz223=on&inclusionDateFrom=&inclusionDateTo=&lastUpdateDateFrom=&lastUpdateDateTo=&sortBy=UPDATE_DATE";
    }

    public function SetDataInput(array $dataInput)
    {
        if (isset($dataInput['inn']))
            $this->data['inn'] = $dataInput['inn'];
    }

    public function Check()
    {
        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        $output = curl_exec($ch);
        curl_close($ch);

        phpQuery::newDocument($output);
        $resultSelector = \PhpQuery\pq('#exceedSphinxPageSizeDiv .registerBoxBank')->length;

        return (!empty($resultSelector)) ? ($resultSelector > 0) : false;
    }
}