<?php
namespace ClientChecker\CheckInstance;

use PhpQuery\PhpQuery as phpQuery;

class Bankrupt implements ICheckInstance
{
    private $data = [
        'inn' => ''
    ];

    public static function Init(array $dataInput)
    {
        $inst = new self();
        $inst->SetDataInput($dataInput);
        return $inst;
    }

    public static function GetMethodName()
    {
        return 'bankrupt';
    }

    public function GetDataUrl()
    {
        return "https://bankrot.fedresurs.ru/DebtorsSearch.aspx";
    }

    public function SetDataInput(array $dataInput)
    {
        if (isset($dataInput['inn']))
            $this->data['inn'] = $dataInput['inn'];
    }

    public function Check()
    {
        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: debtorsearch=typeofsearch=Persons&orgname=&orgaddress=&orgregionid=&orgogrn=&orginn=&orgokpo=&OrgCategory=&prslastname=&prsfirstname=&prsmiddlename=&prsaddress=&prsregionid=&prsinn={$this->data['inn']}&prsogrn=&prssnils=&PrsCategory=&pagenumber=0"));
        $output = curl_exec($ch);
        curl_close($ch);

        return (substr_count($output, $this->data['inn']) <= 1);
    }
}