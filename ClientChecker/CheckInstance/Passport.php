<?php
namespace ClientChecker\CheckInstance;

use ClientChecker\CaptchaResolve;
use PhpQuery\PhpQuery as phpQuery;

class Passport implements ICheckInstance
{
    private $data = [
        'series' => '',
        'number' => '',
    ];

    public static function Init(array $dataInput)
    {
        $inst = new self();
        $inst->SetDataInput($dataInput);
        return $inst;
    }

    public static function GetMethodName()
    {
        return 'passport';
    }

    public function GetDataUrl()
    {
        return "http://services.fms.gov.ru/info-service-result.htm?sid=2000";
    }

    public function SetDataInput(array $dataInput)
    {
        if (isset($dataInput['series']))
            $this->data['series'] = $dataInput['series'];
        if (isset($dataInput['number']))
            $this->data['number'] = $dataInput['number'];
    }

    public function Check()
    {
        //cookie tmp file
        $cookieFile = tempnam(sys_get_temp_dir(), 'PasCookie');
        //get session cookies
        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_exec($ch);
        curl_close($ch);

        //get captcha
        $captchaUrl = 'http://services.fms.gov.ru/services/captcha.jpg';
        $ch = curl_init($captchaUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        $output = curl_exec($ch);
        curl_close($ch);

        $captchaKey = CaptchaResolve::Resolve($output);
        $postBody = [
            'sid' => 2000,
            'form_name' => 'form',
            'DOC_SERIE' => $this->data['series'],
            'DOC_NUMBER' => $this->data['number'],
            'captcha-input' => $captchaKey,
        ];
        $ch = curl_init('http://services.fms.gov.ru/info-service.htm');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postBody));
        $output = curl_exec($ch);
        curl_close($ch);
        unlink($cookieFile);

        return (strpos($output, 'Среди недействительных не значится') == false) ? false : true;
    }
}
