<?php
namespace ClientChecker\CheckInstance;

interface ICheckInstance
{
    public static function Init(array $dataInput);

    public static function GetMethodName();

    public function SetDataInput(array $dataInput);

    public function Check();
}