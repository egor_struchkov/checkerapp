<?php
namespace ClientChecker\CheckInstance;

use ClientChecker\CaptchaResolve;
use PhpQuery\PhpQuery as phpQuery;

class AccBlock implements ICheckInstance
{
    private $data = [
        'inn' => '',
        'bik' => '',
    ];

    public static function Init(array $dataInput)
    {
        $inst = new self();
        $inst->SetDataInput($dataInput);
        return $inst;
    }

    public static function GetMethodName()
    {
        return 'acc_block';
    }

    public function GetDataUrl()
    {
        return "https://service.nalog.ru/bi.do";
    }

    public function SetDataInput(array $dataInput)
    {
        if (isset($dataInput['inn']))
            $this->data['inn'] = $dataInput['inn'];
        if (isset($dataInput['bik']))
            $this->data['bik'] = $dataInput['bik'];
    }

    public function Check()
    {
        //cookie tmp file
        $cookieFile = tempnam(sys_get_temp_dir(), 'DebtCookie');

        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        $output = curl_exec($ch);
        curl_close($ch);
        phpQuery::newDocument($output);
        $captchaSrc = \PhpQuery\pq("#capImg")->attr('src');

        //get captcha
        $ch = curl_init('https://service.nalog.ru/' . $captchaSrc);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        $output = curl_exec($ch);
        curl_close($ch);
        $captchaKey = CaptchaResolve::Resolve($output);

        $postBody = [
            'c' => 'search',
            'requestType' => 'FINDPRS',
            'innPRS' => $this->data['inn'],
            'bikPRS' => $this->data['bik'],
            'cap' => $captchaKey
        ];
        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postBody));
        $output = curl_exec($ch);
        curl_close($ch);
        unlink($cookieFile);

        return (strpos($output, 'Действующие решения о приостановлении по указанному налогоплательщику ОТСУТСТВУЮТ') == false) ? false : true;

    }
}
