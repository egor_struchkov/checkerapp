<?php
namespace ClientChecker\CheckInstance;

use PhpQuery\PhpQuery as phpQuery;

class Msp implements ICheckInstance
{
    private $data = [
        'inn' => ''
    ];

    public static function Init(array $dataInput)
    {
        $inst = new self();
        $inst->SetDataInput($dataInput);
        return $inst;
    }

    public static function GetMethodName()
    {
        return 'msp';
    }

    public function GetDataUrl()
    {
        return "https://ofd.nalog.ru/search-proc.json";
    }

    public function SetDataInput(array $dataInput)
    {
        if (isset($dataInput['inn']))
            $this->data['inn'] = $dataInput['inn'];
    }

    public function Check()
    {
        $postBody = [
            'query' => $this->data['inn'],
        ];
        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postBody));
        $output = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($output, true);

        return (!empty($result['data'])) ? (is_array($result['data']) && count($result['data']) > 0) : false;
    }
}