<?php
namespace ClientChecker\CheckInstance;

use ClientChecker\CaptchaResolve;
use PhpQuery\PhpQuery as phpQuery;

class Debt implements ICheckInstance
{
    private $data = [
        'inn' => '',
    ];

    public static function Init(array $dataInput)
    {
        $inst = new self();
        $inst->SetDataInput($dataInput);
        return $inst;
    }

    public static function GetMethodName()
    {
        return 'debt';
    }

    public function GetDataUrl()
    {
        return "https://service.nalog.ru/zd.do";
    }

    public function SetDataInput(array $dataInput)
    {
        if (isset($dataInput['inn']))
            $this->data['inn'] = $dataInput['inn'];
    }

    public function Check()
    {
        //cookie tmp file
        $cookieFile = tempnam(sys_get_temp_dir(), 'DebtCookie');

        $ch = curl_init($this->GetDataUrl());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        $output = curl_exec($ch);
        curl_close($ch);
        phpQuery::newDocument($output);
        $captchaToken = \PhpQuery\pq("input[name=captchaToken]")->val();

        //get captcha
        $ch = curl_init("https://service.nalog.ru/static/captcha.html?a={$captchaToken}");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        $output = curl_exec($ch);
        curl_close($ch);
        $captchaKey = CaptchaResolve::Resolve($output);

        $postBody = [
            'inn' => $this->data['inn'],
            'captcha' => $captchaKey,
            'captchaToken' => $captchaToken,
        ];
        $ch = curl_init('https://service.nalog.ru/zd-proc.do');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postBody));
        $output = curl_exec($ch);
        curl_close($ch);
        unlink($cookieFile);
        $answer = json_decode($output, true);

        return (strpos($answer['text'], 'не имеет превышающую 1000 рублей задолженность') == false) ? false : true;
    }
}
